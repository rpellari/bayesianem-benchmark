import IMP
import RMF
import IMP.atom
import IMP.rmf
import IMP.pmi
import IMP.pmi.topology
import IMP.pmi.dof
import IMP.pmi.macros
import IMP.pmi.restraints
import IMP.pmi.restraints.stereochemistry
import IMP.bayesianem
import IMP.bayesianem.restraint
import IMP.bayesianem.movers
import tempfile,os
import sys
import numpy as np
import math
import itertools
import random
   
output_objects=[]

pdb_id=sys.argv[1]
root_dir='/baycells/scratch/shanot/EM_benchmark/'
output_dir='%s/modeling/%s/'%(root_dir, pdb_id)
gmm_file='%s/data/%s/%s_imp.gmm'%(root_dir, pdb_id, pdb_id)
output_prefix=""
if len(sys.argv)>2:
    output_prefix=sys.argv[2]
EV_weight=1.0
print output_prefix, EV_weight

pdb_id=pdb_id.split('_')[0]


###################### SYSTEM SETUP #####################
# Read sequences etc
# The TopologyReader reads the text file, and the BuildSystem macro constructs it
mdl = IMP.Model()
reader = IMP.pmi.topology.TopologyReader('%s/%s_topology.dat'%(output_dir, pdb_id),
                                         pdb_dir =   '%s/data/%s'%(root_dir, pdb_id),
                                         fasta_dir = '%s/data/%s'%(root_dir, pdb_id),
                                         gmm_dir =   '%s/gmm'%(output_dir))

bs = IMP.pmi.macros.BuildSystem(mdl)
bs.add_state(reader) # note you can call this multiple times to create a multi-state system
hier, dof = bs.execute_macro(max_rb_trans=4.0, max_rb_rot=0.4, max_bead_trans=4.0, max_srb_trans=4.0,max_srb_rot=0.4)

import IMP.rmf
import RMF

pm=IMP.bayesianem.movers.PCAMover(mdl,hier,resolution=10)

# Connectivity keeps things connected along the backbone (ignores if inside same rigid body)
crs = []
mols = IMP.pmi.tools.get_molecules(hier)
for mol in mols:
    IMP.pmi.tools.display_bonds(mol)
    cr = IMP.pmi.restraints.stereochemistry.ConnectivityRestraint(mol)
    cr.add_to_model()
    cr.set_label(mol.get_name())
    output_objects.append(cr)
    crs.append(cr)

# Excluded volume - automatically more efficient due to rigid bodies
evr = IMP.pmi.restraints.stereochemistry.ExcludedVolumeSphere(included_objects = hier)
evr.add_to_model()
evr.set_weight(EV_weight)
output_objects.append(evr)


# EM restraint
densities = IMP.atom.Selection(hier,representation_type=IMP.atom.DENSITIES).get_selected_particles()
# for EM maps simulated from pdbs with gaps, we select only the Gaussians that belong to that pdb
pdb_densities = []
for p in densities:
    if IMP.core.RigidBody.get_is_setup(p) and not IMP.core.NonRigidMember.get_is_setup(p):
        pdb_densities.append(p)
   
gem = IMP.bayesianem.restraint.GaussianEMRestraintWrapper(pdb_densities,target_fn=gmm_file,
                                            scale_target_to_mass=True,
                                            slope=0.01,
                                            target_radii_scale=3.0,
                                            target_is_rigid_body=False)
gem.set_label("EM")
gem.add_target_density_to_hierarchy(hier)
gem.add_to_model()
output_objects.append(gem)

# RMSD

rmsd=IMP.pmi.analysis.RMSD(hier,hier,[mol.get_name() for mol in mols],dynamic0=True,dynamic1=False)
output_objects.append(rmsd)

IMP.pmi.tools.shuffle_configuration(hier.get_children()[0],
                                    max_translation=100)

# Quickly move all flexible beads into place
dof.optimize_flexible_beads(10)

num_frames=20000

try:
    import IMP.mpi
    print('ReplicaExchange: MPI was found. Using Parallel Replica Exchange')
    rex_obj = IMP.mpi.ReplicaExchange()
except ImportError:
    print('ReplicaExchange: Could not find MPI. Using Serial Replica Exchange')
    rex_obj = _SerialReplicaExchange()

rbrots=IMP.pmi.dof.create_rigid_body_movers(dof,0.0,0.4)
allmovers=dof.get_movers()+[pm]

weights =     [1,        ]  *100
evr_weights = [1,        ]  *100
movers =      [allmovers ]  *100
numframes=    [200,      ]  *100
tags=         ["hw_all_hev"]*100

for i,w in enumerate(weights):
    gem.set_weight(w)
    evr.set_weight(evr_weights[i])
    mvs=movers[i]
    tag=tags[i]
    global_output_directory='%s/%s_output_%d_%s/'%(output_dir,output_prefix, i,tag)
    nf=numframes[i]

    
    rex=IMP.pmi.macros.ReplicaExchange0(mdl,
                                        root_hier=hier,                          # pass the root hierarchy
                                        monte_carlo_sample_objects=mvs,  # pass MC movers
                                        global_output_directory=global_output_directory,
                                        output_objects=output_objects,
                                        monte_carlo_steps=10,
					replica_exchange_minimum_temperature=1.0,
                                        replica_exchange_maximum_temperature=2.5,
		                        replica_exchange_swap=True,
					save_coordinates_mode="50th_score",
                                        number_of_best_scoring_models=0,      # set >0 to store best PDB files (but this is slow to do online)
                                        number_of_frames=nf,                   # increase number of frames to get better results!
                                        replica_exchange_object=rex_obj)
    rex.execute_macro()
