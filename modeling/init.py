import IMP
import RMF
import IMP.atom
import IMP.rmf
import IMP.pmi
import IMP.pmi.topology
import IMP.pmi.plotting
import IMP.pmi.plotting.topology
import IMP.pmi.dof
import IMP.pmi.macros
import IMP.pmi.restraints
import IMP.pmi.restraints.stereochemistry
import IMP.bayesianem
import IMP.bayesianem.restraint
import tempfile,os
import sys
import numpy as np


output_objects=[]

pdb_id=sys.argv[1]
root_dir='/baycells/scratch/shanot/EM_benchmark/'
output_dir='%s/modeling/%s/'%(root_dir, pdb_id)
gmm_file='%s/data/%s/%s_imp.gmm'%(root_dir, pdb_id, pdb_id)


###################### SYSTEM SETUP #####################
# Read sequences etc
# The TopologyReader reads the text file, and the BuildSystem macro constructs it
mdl = IMP.Model()
reader = IMP.pmi.topology.TopologyReader('%s/modeling/%s/%s_topology.dat'%(root_dir, pdb_id, pdb_id),
                                         pdb_dir =   '%s/data/%s'%(root_dir, pdb_id),
                                         fasta_dir = '%s/data/%s'%(root_dir, pdb_id),
                                         gmm_dir =   '%s/gmm'%(output_dir))

bs = IMP.pmi.macros.BuildSystem(mdl)
bs.add_state(reader) # note you can call this multiple times to create a multi-state system
hier, dof = bs.execute_macro()
IMP.pmi.plotting.topology.draw_component_composition(dof)


# Connectivity keeps things connected along the backbone (ignores if inside same rigid body)
crs = []
moldict = bs.get_molecules()[0]
mols = []
for molname in moldict:
    for mol in moldict[molname]:
        IMP.pmi.tools.display_bonds(mol)
        cr = IMP.pmi.restraints.stereochemistry.ConnectivityRestraint(mol)
        cr.add_to_model()
        cr.set_label(molname)
        output_objects.append(cr)
        crs.append(cr)
        mols.append(mol)

# Excluded volume - automatically more efficient due to rigid bodies
evr = IMP.pmi.restraints.stereochemistry.ExcludedVolumeSphere(included_objects = mols)
evr.add_to_model()
output_objects.append(evr)


# EM restraint
densities = IMP.atom.Selection(hier,representation_type=IMP.atom.DENSITIES).get_selected_particles()
gem = IMP.bayesianem.restraint.GaussianEMRestraintWrapper(densities,target_fn=gmm_file,
                                            scale_target_to_mass=True,
                                            slope=0.01,
                                            target_radii_scale=3.0,
                                            target_is_rigid_body=False)
gem.set_label("EM")
gem.add_target_density_to_hierarchy(hier)
gem.add_to_model()
output_objects.append(gem)

dof.optimize_flexible_beads(1000)
score=IMP.pmi.tools.get_restraint_set(mdl).unprotected_evaluate(None)
em_score=gem.rs.unprotected_evaluate(None)
print("score=%e"%(score))
print("em score=%e"%(em_score))
with open("%s/ref.rmf.score"%(output_dir), "w") as f:
    f.write("Total_Score=%e\n"%(score))
    f.write("EM_Score=%e\n"%(em_score))
rh = RMF.create_rmf_file("%s/ref.rmf"%(output_dir))
IMP.rmf.add_hierarchies(rh, [hier])
IMP.rmf.save_frame(rh)
del rh
