#!/bin/bash
#SBATCH --job-name=B_XXXX_YYYY

source ~/modules.sh

mpirun -n 48 ~/imp-fast/setup_environment.sh python model.py XXXX YYYY
