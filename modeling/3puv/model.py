import IMP
import RMF
import IMP.atom
import IMP.rmf
import IMP.pmi
import IMP.pmi.topology
import IMP.pmi.dof
import IMP.pmi.macros
import IMP.pmi.restraints
import IMP.pmi.restraints.stereochemistry
import IMP.pmi.restraints.crosslinking
import IMP.bayesianem
import IMP.bayesianem.restraint
import IMP.bayesianem.movers
import tempfile,os
import sys
import numpy as np

try:
    import IMP.mpi
    print('ReplicaExchange: MPI was found. Using Parallel Replica Exchange')
    rex_obj = IMP.mpi.ReplicaExchange()
except ImportError:
    print('ReplicaExchange: Could not find MPI. Using Serial Replica Exchange')
    rex_obj = _SerialReplicaExchange()


output_prefix=sys.argv[1]
step=int(sys.argv[2])
maxstep=int(sys.argv[3])

output_objects=[]


###################### SYSTEM SETUP #####################
# Read sequences etc
# Normally the topology table is kept in a text file but here we just write it to a temporary one
# The TopologyReader reads the text file, and the BuildSystem macro constructs it
mdl = IMP.Model()
reader = IMP.pmi.topology.TopologyReader('3puv_topology.dat',
                                         pdb_dir = '../../data/3puv',
                                         fasta_dir = '../../data/3puv',
                                         gmm_dir = 'gmm/')
bs = IMP.pmi.macros.BuildSystem(mdl)
bs.add_state(reader) # note you can call this multiple times to create a multi-state system
hier, dof = bs.execute_macro(max_rb_trans=4.0, max_rb_rot=0.1, max_bead_trans=4.0, max_srb_trans=4.0,max_srb_rot=0.1)

#pm=IMP.bayesianem.movers.PCAMover(mdl,hier,resolution=10)

# Connectivity keeps things connected along the backbone (ignores if inside same rigid body)
mols=IMP.pmi.tools.get_molecules(hier)
connected_pairs={}
for mol in mols:
    molname = mol.get_name() + str(IMP.atom.Copy(mol).get_copy_index())
    IMP.pmi.tools.display_bonds(mol)
    cr = IMP.pmi.restraints.stereochemistry.ConnectivityRestraint(mol,scale=1.0)
    cr.add_to_model()
    cr.set_label("connect_"+molname)
    output_objects.append(cr)
    connected_pairs[mol]=cr.get_particle_pairs()

# Excluded volume - automatically more efficient due to rigid bodies
evr = IMP.pmi.restraints.stereochemistry.ExcludedVolumeSphere(included_objects = mols)
evr.add_to_model()
output_objects.append(evr)

sel = IMP.atom.Selection(hierarchy=hier, representation_type=IMP.atom.DENSITIES)
densities=sel.get_selected_particles()

steps=range(1,maxstep)
steps=steps+[i for i in reversed(steps[1:-1])]
gmmnum=steps[(step-1)%len(steps)]
print steps, step, gmmnum
gem = IMP.bayesianem.restraint.GaussianEMRestraintWrapper(densities,
                                                 target_fn='../../data/3puv/%d_imp.gmm'%(gmmnum),
                                                 scale_target_to_mass=True,
                                                 slope=0.01,
                                                 target_radii_scale=3.0,
                                                 target_is_rigid_body=False)
gem.add_to_model()
gem.set_label("Total")
output_objects.append(gem)

###################### SAMPLING #####################
# First shuffle the system
# Mind that you cannod shuffle the root hier, as it have the gaussian beads attached to
# by-state shuffling is safer

if step==1:
    for state in bs.system.get_states():
        IMP.pmi.tools.shuffle_configuration(state.get_hierarchy(),
                                            max_translation=500)
    dof.optimize_flexible_beads(10)
else:
    replica_number = rex_obj.get_my_index()
    rh_ref = RMF.open_rmf_file_read_only('%sseed_%d.rmf3'%(output_prefix,step-1))
    IMP.rmf.link_hierarchies(rh_ref, [hier])
    IMP.rmf.load_frame(rh_ref, RMF.FrameID(replica_number))

output_dir='.'
global_output_directory='%s/%s_output_%d_%d/'%(output_dir,output_prefix, step, gmmnum)

nf=1000

rex=IMP.pmi.macros.ReplicaExchange0(mdl,
                                    root_hier=hier,                          # pass the root hierarchy
                                    monte_carlo_sample_objects=dof.get_movers(),  # pass MC movers
                                    global_output_directory=global_output_directory,
                                    rmf_output_objects=output_objects,
                                    output_objects=None,
                                    monte_carlo_steps=10,
                                    replica_exchange_minimum_temperature=1.0,
                                    replica_exchange_maximum_temperature=2.5,
                                    replica_exchange_swap=True,
                                    save_coordinates_mode="50th_score",
                                    number_of_best_scoring_models=0,      # set >0 to store best PDB files (but this is slow to do online)
                                    number_of_frames=nf,                   # increase number of frames to get better results!
                                    replica_exchange_object=rex_obj)
rex.execute_macro()
