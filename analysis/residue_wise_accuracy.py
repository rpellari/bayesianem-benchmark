import IMP
import RMF
import IMP.atom
import IMP.rmf
import IMP.pmi
import IMP.pmi.topology
import IMP.pmi.dof
import IMP.pmi.macros
import IMP.pmi.restraints
import IMP.pmi.restraints.stereochemistry
import IMP.bayesianem
import IMP.bayesianem.restraint
import tempfile,os
import sys
import glob
import itertools
import math
import numpy as np

class Cluster(object):
    def __init__(self,rmf_file, rmf_frame, score, rmsd):
	self.members=[]
        self.add_member(rmf_file, rmf_frame, score, rmsd)
    def add_member(self,rmf_file, rmf_frame, score, rmsd):
        self.members.append((rmf_file, rmf_frame, score, rmsd))
	self.members.sort(key=lambda t: t[2])
    def get_best_member(self):
        return self.members[0]


class ClusterDict(object):
    def __init__(self, cluster):
        self.dict={0:cluster}
    def add_cluster(self,cluster):
        new_key=max(self.dict.keys())+1
        self.dict[new_key]=cluster

def find_mol_by_name(hier,k):
    molname, copy_num = k.split('.')
    copy_num=int(copy_num)
    for mol in hier.get_children()[0].get_children():
        if molname == mol.get_name() and copy_num == IMP.atom.Copy(mol).get_copy_index():
            return mol
    return None



def get_num_residues(state):
    N=0
    for mol in state.get_children():
        name=mol.get_name()
        parts=True
        i=1
        while parts:
            sel=IMP.atom.Selection(mol,residue_index=i,representation_type=IMP.atom.BALLS,resolution=1)
            parts=sel.get_selected_particles()
    	    if parts:
    	        i=i+1
        N+=i-1
    return N

def get_moldict_xyzdict(state, dist_dict=None):
    moldict={}
    mol_xyzs={}
    added=[]
    for mol in state.get_children():
        print mol.get_name(), mol
        name=mol.get_name()
        parts=True
        mol_xyzs[mol]=[]
        i=1
        while parts:
            sel=IMP.atom.Selection(mol,residue_index=i,representation_type=IMP.atom.BALLS,resolution=1)
            parts=sel.get_selected_particles()
    	    if parts:
                mol_xyzs[mol].append(IMP.core.XYZ(parts[0]))
    	        i=i+1
        mols=[mol]
        if dist_dict is not None:
            for k1,k2 in dist_dict.keys():
                if find_mol_by_name(state.get_parent(),k1)==mol:
                    mols.append(find_mol_by_name(state.get_parent(),k2))
        if not mol in added:
            if name not in moldict:
                moldict[name]=[]
            moldict[name]+=list(set(mols))
            added+=list(set(mols))
    print moldict
    return moldict, mol_xyzs

def get_xyzs(mol):
    i=1
    mol_xyzs=[]
    parts=True
    while parts:
        sel=IMP.atom.Selection(mol,residue_index=i,representation_type=IMP.atom.BALLS,resolution=1)
        parts=sel.get_selected_particles()
        if parts:
            mol_xyzs.append(IMP.core.XYZ(parts[0]))
            i=i+1
    return mol_xyzs
    
def make_xyzs_pairs_dict(dist_dict, hier1, hier2):
    xyzs_pairs_dict={}
    if dist_dict is not None:
        for (k1,k2), index_dict in dist_dict.iteritems():
            m1 = find_mol_by_name(hier1,k1)
            m2 = find_mol_by_name(hier2,k2)
            parts_m1=IMP.atom.Selection(m1,residue_indexes=index_dict.keys(),resolution=1,representation_type=IMP.atom.BALLS).get_selected_particles()
            parts_m2=IMP.atom.Selection(m2,residue_indexes=index_dict.values(),resolution=1,representation_type=IMP.atom.BALLS).get_selected_particles()
            xyzs1 = [IMP.core.XYZ(p) for p in parts_m1]
            xyzs2 = [IMP.core.XYZ(p) for p in parts_m2]
            xyzs_pairs_dict[(m1,m2)]=(xyzs1, xyzs2)
    return xyzs_pairs_dict


def get_permutation_with_lowest_rmsd(ref_mols, mols, xyzs_pairs_dict=None):
    mols_list=[]
    rmsd=[]
    for rmf_mols in itertools.permutations(mols):
        ref_xyzs=[]
        rmf_xyzs=[]
        for (m1,m2) in zip(ref_mols, rmf_mols):
            if (m1,m2) in xyzs_pairs_dict:
                   ref_xyzs+=xyzs_pairs_dict[(m1,m2)][0] 
                   rmf_xyzs+=xyzs_pairs_dict[(m1,m2)][1] 
            else:
                  ref_xyzs += get_xyzs(m1)
                  rmf_xyzs += get_xyzs(m2)
        r=IMP.atom.get_rmsd(rmf_xyzs, ref_xyzs)
        rmsd.append(r)
        mols_list.append(rmf_mols)
    m=min(rmsd)
    return mols_list[rmsd.index(m)],m

output_objects=[]


pdb_id=sys.argv[1]
rmf_file=sys.argv[2]
frame_idx=int(sys.argv[3])
if len(sys.argv)>4:
    dist_dict_file=sys.argv[4]

dist_dict=None
try:
    with open(dist_dict_file, 'r') as f:
        c=""
        for line in f:
	    c=line
        dist_dict=eval(c)
except:
    pass




root_dir='/baycells/scratch/shanot/EM_benchmark/'
output_dir='%s/modeling/%s/'%(root_dir, pdb_id)


mdl = IMP.Model()
ref_fh = RMF.open_rmf_file_read_only("%s/ref.rmf"%(output_dir))
ref_h=IMP.rmf.create_hierarchies(ref_fh, mdl)
ref_state = ref_h[0].get_children()[0]

ref_gmm_sel=IMP.atom.Selection(ref_state, representation_type=IMP.atom.DENSITIES)
ref_gaussians=ref_gmm_sel.get_selected_particles()

ref_moldict, ref_xyzs = get_moldict_xyzdict(ref_state, dist_dict)

rmf_hh=None

distances={}
m=None
M=None
print(rmf_file, frame_idx)
rmf_fh = RMF.open_rmf_file_read_only(rmf_file)
num_frames = rmf_fh.get_number_of_frames()
rmf_state=None
rmf_hh=None
xyzs_pairs_dict={}
if num_frames>0:
    if rmf_hh is None:
        rmf_hh = IMP.rmf.create_hierarchies(rmf_fh, mdl)
        rmf_state = rmf_hh[0].get_children()[0]
        gmm_sel=IMP.atom.Selection(rmf_state, representation_type=IMP.atom.DENSITIES)
        rmf_gaussians=gmm_sel.get_selected_particles()
        rmf_moldict, rmf_xyzs = get_moldict_xyzdict(rmf_state, dist_dict)
        xyzs_pairs_dict = make_xyzs_pairs_dict(dist_dict, ref_h[0], rmf_hh[0])
    else:
        IMP.rmf.link_hierarchies(rmf_fh, rmf_hh)
    try:
        IMP.rmf.load_frame(rmf_fh, RMF.FrameID(frame_idx))
    except:
        print("skipping frame %s:%d\n"%(rmf_file, frame_idx))

    #compare to reference structure
    rmsd_dict={}
    position_dict={} #the postitional error of the center of mass of subunits
    angular_dict={} #the angular error of the center of mass of subunits
    perc_dict={} # the percentage of CAs withing 10A of the reference position
    total_rmsd=0.0
    total_perc=0.0
    total_N=0
    for molname, ref_mols in ref_moldict.iteritems():
        print molname
        rmf_mols_best_order, m = get_permutation_with_lowest_rmsd(ref_mols,rmf_moldict[molname], xyzs_pairs_dict)
        for n, (ref_mol,rmf_mol) in enumerate(zip(ref_mols,rmf_mols_best_order)):
            ref_xyzs=[]
            rmf_xyzs=[]
            if (ref_mol,rmf_mol) in xyzs_pairs_dict:
                       print "getting",ref_mol, "and", rmf_mol , "from moldict"
                       ref_xyzs=xyzs_pairs_dict[(ref_mol,rmf_mol)][0] 
                       rmf_xyzs=xyzs_pairs_dict[(ref_mol,rmf_mol)][1] 
            else:
                      ref_xyzs = get_xyzs(ref_mol)
                      rmf_xyzs = get_xyzs(rmf_mol)

            for i, (x1,x2) in enumerate(zip(ref_xyzs, rmf_xyzs)):
                dx=IMP.core.get_distance(x1, x2)
                distances[(rmf_mol,i)]=dx
                if dx<m or m is None:
                    m=dx
                if dx>M or M is None:
                    M=dx
print m,M
for (mol, res_idx), dx in distances.iteritems():
    bead=IMP.atom.Selection(mol,residue_index=res_idx,representation_type=IMP.atom.BALLS).get_selected_particles()
    #color = IMP.display.get_rgb_color((dx-m)/(M-m))
    c=0
    if dx<10:
	c=0
    elif dx<20:
	c=0.5
    else:
        c=1
    color = IMP.display.get_rgb_color(c)
    for b in bead:
        IMP.display.Colored(b).set_color(color)

outfile="%s/colored.rmf"%(output_dir)
if dist_dict is not None:
    outfile="%s/colored_dist_dict.rmf"%(output_dir)
rh = RMF.create_rmf_file(outfile)
IMP.rmf.add_hierarchies(rh, rmf_hh)
IMP.rmf.save_frame(rh)
del rh
