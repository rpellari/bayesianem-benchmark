
import IMP
import IMP.rmf
import RMF
import IMP.display
import scipy
from scipy import interpolate
import numpy as np
import IMP.container
import sys

rmf_file=sys.argv[1]

class MiniNetwork(object):


    import itertools
    
    def __init__(self):
        self.edges={}
        self.nodes=set()
        self.networks={}
        self.networks_status={}
    
    def add_edge(self,pair):
        
        ps=list(pair)
        if pair[0] not in self.edges:
           self.edges[pair[0]]=set([pair[1]])
        else:
           self.edges[pair[0]].add(pair[1])

        if pair[1] not in self.edges:
           self.edges[pair[1]]=set([pair[0]])
        else:
           self.edges[pair[1]].add(pair[0])
           
        self.nodes.add(pair[0])
        self.nodes.add(pair[1])
        
        attribute_0=None
        attribute_1=None
        
        for k in self.networks:
            if pair[0] in self.networks[k]: attribute_0=k
            if pair[1] in self.networks[k]: attribute_1=k
        
        if attribute_0 is None and attribute_1 is None:
            nindex=self.get_number_of_networks()
            self.networks[nindex]=set(pair)
            
        elif attribute_0 is not None and attribute_1 is None:
            self.networks[attribute_0]|=set(pair)

        elif attribute_0 is None and attribute_1 is not None:
            self.networks[attribute_1]|=set(pair)        

        elif attribute_0 is not None and attribute_1 is not None:
            if attribute_0 != attribute_1:
                 self.networks[attribute_0]|=set(pair)
                 self.networks[attribute_0]|=self.networks[attribute_1]
                 self.networks.pop(attribute_1)
            else:
                 self.networks[attribute_0]|=set(pair)
        
        #self.networks_status[nindex]="ACTIVE"
        #self.update_network(0)
        #self.purge_network()
    
    def get_number_of_networks(self):
        return len(self.networks.keys())
    
    def purge_network(self):
        for k in self.networks_status.keys():
            if self.networks_status[k] == "DELETE":
                self.networks.pop(k)
                self.networks_status.pop(k)
    
    def update_network(self,iter):
        iter+=1
        for (k,j) in list(self.itertools.combinations(self.networks.keys(), 2)):
            if self.networks_status[j]!="DELETE" and self.networks_status[k]!="DELETE":
                for item in self.networks[j]:
                    if item in self.networks[k]:
                        self.networks[k]|=self.networks[j]
                        self.networks_status[j]="DELETE"
                        self.update_network(iter)
    
    def get_networks(self):
        networks=[]
        for k in self.networks:
            netws=list(self.networks[k])
            networks.append(self.order_network(netws))
        return networks


    def order_network(self,inputlist):
        ordered_list=[]
        nstart=None
        print "NEW SEQUENCE"
        for n in inputlist:
            print n,len(self.edges[n])
            if len(self.edges[n])==1:
               print "GOT IT"
               nstart=n
               break
        if not nstart: return []
        oldedges=list(self.edges[nstart])
        oldedge=oldedges[0]
        oldedgem1=nstart
        nedges=len(self.edges[oldedge])
        ordered_list.append(oldedgem1)
        ordered_list.append(oldedge)
        while nedges==2:
            newedges=list(self.edges[oldedge])
            nedges=len(newedges)
            if nedges == 1: break
            newedges.remove(oldedgem1)
            oldedgem1=oldedge
            oldedge=list(newedges)[0]
            ordered_list.append(oldedge)
        return ordered_list

            
            
#test mini network

mn=MiniNetwork()
"""
mn.add_edge((1,2))
print mn.get_networks()
mn.add_edge((4,3))
print mn.get_networks()


mn.add_edge((2,3))
print mn.get_networks()

exit()
"""
m=IMP.Model()

rh = RMF.open_rmf_file_read_only(rmf_file)
prot = IMP.rmf.create_hierarchies(rh, m)[0]
IMP.rmf.link_hierarchies(rh, [prot])
IMP.rmf.load_frame(rh, RMF.FrameID(0))
del rh

sel=IMP.atom.Selection(prot, resolution=1, state_index=0)
sel-=IMP.atom.Selection(prot, molecule='Rrp46_gfp', residue_indexes=range(247, 1000))
ps = [p for p in sel.get_selected_particles() if IMP.core.XYZR.get_is_setup(p)]



particle_pairs=[]

for n,p in enumerate(ps):
    #if IMP.atom.Bond.get_is_setup(p): print p, "BOND"
    print "atom",n 
    if IMP.atom.Bonded.get_is_setup(p): 
       for i in range(IMP.atom.Bonded(p).get_number_of_bonds()):
           mn.add_edge((p.get_index(),IMP.atom.Bonded(p).get_bonded(i).get_particle().get_index()))
           
nets=mn.get_networks()

print nets

hbase=IMP.atom.Hierarchy(IMP.Particle(m))

rh = RMF.create_rmf_file("%s_rendered.rmf3"%(rmf_file))


for net in nets:

    coords=[]
    radii=[]
    for n,index in enumerate(net):
        coor=list(IMP.core.XYZ(m,index).get_coordinates())
        rad=[IMP.core.XYZR(m,index).get_radius()]
        color=IMP.display.Colored(m,index).get_color()
        rgb=[color.get_red(),color.get_green(),color.get_blue()]
        coords.append(coor+rad+rgb)
        #radii.append([IMP.core.XYZR(m,index).get_radius(),float(n)])

    data=np.array(coords)

    #This is your data, but we're 'zooming' into just 5 data points
    #because it'll provide a better visually illustration
    #also we need to transpose to get the data in the right format
    data = data.transpose()

    #now we get all the knots and info about the interpolated spline
    try:
       tck, u= interpolate.splprep(data,s=1.0)
    except:
       continue
       
    #here we generate the new interpolated dataset, 
    #increase the resolution by increasing the spacing, 500 in this example
    new = interpolate.splev(np.linspace(0,1,len(coords)*2), tck)

    #radiinp=np.array(radii)
    #radiinp=radiinp.transpose()

    #tck1, u1= interpolate.splprep(radiinp,s=0.1)
    #newradii = interpolate.splev(np.linspace(0,1,len(radiinp)), tck1)
    
    sgs=[]
    for n in range(1,len(new[0])):
        xn=new[0][n]
        yn=new[1][n]
        zn=new[2][n]
        xnm1=new[0][n-1]
        ynm1=new[1][n-1]
        znm1=new[2][n-1]
        radn=new[3][n]
        radnm1=new[3][n-1]
        avrad=(radn+radnm1)/2.0/2.0
        redn=new[4][n]
        rednm1=new[4][n-1]
        greenn=new[5][n]
        greennm1=new[5][n-1]   
        bluen=new[6][n]
        bluenm1=new[6][n-1]  
        avred=(redn+rednm1)/2.0
        avgreen=(greenn+greennm1)/2.0
        avblue=(bluen+bluenm1)/2.0
        c=IMP.display.Color(avred,avgreen,avblue)
        #sgs.append(IMP.display.SegmentGeometry(IMP.algebra.Segment3D(IMP.algebra.Vector3D(xn,yn,zn),IMP.algebra.Vector3D(xnm1,ynm1,znm1))))
        
        #cyl=IMP.algebra.Cylinder3D(IMP.algebra.Segment3D(IMP.algebra.Vector3D(xn,yn,zn),IMP.algebra.Vector3D(xnm1,ynm1,znm1)),avrad)
        #sgs.append(IMP.display.CylinderGeometry(cyl,c))
        seg=IMP.algebra.Segment3D(IMP.algebra.Vector3D(xn,yn,zn),IMP.algebra.Vector3D(xnm1,ynm1,znm1))
        sgs.append(IMP.display.SegmentGeometry(seg,c))
    
    '''
    hroot=IMP.atom.Hierarchy(IMP.Particle(m))
    hbase.add_child(hroot)

    xn=new[0][1]
    yn=new[1][1]
    zn=new[2][1]
    radn=new[3][1]
    redn=new[4][1]
    greenn=new[5][1] 
    bluen=new[6][1]
    c=IMP.display.Color(avred,avgreen,avblue)
    p=IMP.Particle(m)
    xyzr=IMP.core.XYZR.setup_particle(p)
    xyzr.set_coordinates((xn,yn,zn))
    xyzr.set_radius(radn)  
    colored=IMP.display.Colored.setup_particle(p,c)
    h=IMP.atom.Hierarchy.setup_particle(p)
    hroot.add_child(h)
    IMP.atom.Bonded.setup_particle(p)
    
    particles=[p]
    
    for n in range(1,len(new[0])):
        xn=new[0][n]
        yn=new[1][n]
        zn=new[2][n]
        radn=new[3][n]
        redn=new[4][n]
        greenn=new[5][n] 
        bluen=new[6][n]
        c=IMP.display.Color(avred,avgreen,avblue)
        p=IMP.Particle(m)
        xyzr=IMP.core.XYZR.setup_particle(p)
        xyzr.set_coordinates((xn,yn,zn))
        xyzr.set_radius(radn)  
        colored=IMP.display.Colored.setup_particle(p,c)
        h=IMP.atom.Hierarchy.setup_particle(p)
        IMP.atom.Bonded.setup_particle(p)
        hroot.add_child(h)
        IMP.atom.Bonded.setup_particle(p)
        particles.append(p)
        IMP.atom.create_bond(
                    IMP.atom.Bonded(p),
                    IMP.atom.Bonded(particles[n-1]),
                    1)
    '''
    IMP.rmf.add_geometries(rh, sgs)



IMP.rmf.add_hierarchies(rh, [hbase])
IMP.rmf.save_frame(rh)
del rh

